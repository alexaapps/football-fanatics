const Alexa = require('ask-sdk');
const quiz = require('./FutQuiz.js');
var TwitterHelper = require('./twitterhelper');

const skillBuilder = Alexa.SkillBuilders.custom();

let skill;
// 2. Skill exports
// ------------------------------------------------------------------------

exports.handler = async function (event, context) {
    console.log('exports');
    if (!skill) {
        skill = Alexa.SkillBuilders.custom()
            .addRequestHandlers(
                LaunchHandler,
                YesHandler,
                QuizHandler,
                AnswerHandler,
                TweetHandler,
                RepeatHandler,
                EndHandler,
                AgainHandler,
                HelpHandler,
                ExitHandler,
                SessionEndedHandler,
                FallbackHandler
            )
            .addErrorHandlers(ErrorHandler)
            .create();
    }

    return skill.invoke(event, context);
}

const states = {
    LAUNCH: `_LAUNCH`,
    YES: `_YES`,
    QUIZ: `_QUIZ`,
    ANSWER: `_ANSWER`,
    END: `_END`,
    AGAIN: `_AGAIN`,
    REPEAT: `_REPEAT`,
    EXIT: `_EXIT`,
    HELP: `_HELP`,
};
// 1. Skill handlers
// --------------------------------------------------------------------
const LaunchHandler = {
    canHandle(handlerInput) {
        console.log('inside launch can request');
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        console.log(' accessing launch request');
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;
        const sessionAttributes = attributesManager.getSessionAttributes();

        const requestAttributes = attributesManager.getRequestAttributes();
        sessionAttributes.states = states.LAUNCH;
        if (!handlerInput.requestEnvelope.session.user.accessToken || handlerInput.requestEnvelope.session.user.accessToken == null || handlerInput.requestEnvelope.session.user.accessToken == undefined ||handlerInput.requestEnvelope.session.user.accessToken === "") {
            speechOutput = `Welcome to football fanatics. Please check your companion app to link your twitter account. Please Linkup to access all features of the skill . <break time="0.25s"/> How well do you think, you know about your favorite football club ?. Do think you are a true football fanatic ?. Say yes, if you want to play. <break time="0.25s"/> Else say exit.`
            var reprompt = `Say, yes to continue. `
            
            return responseBuilder.withLinkAccountCard().speak(speechOutput).reprompt(reprompt).withShouldEndSession(false).getResponse();
                

        }else{

             speechOutput = `Welcome to football fanatics. How well do you think, you know about your favorite football club ?. Do think you are a true football fanatic ?. Say yes, if you want to play.  `;
             var reprompt = 'Say yes to continue. ';
             return responseBuilder
             .speak(speechOutput)
             .reprompt(reprompt)
             .withShouldEndSession(false)
             .getResponse();
            }
     

            }
       
       
};

const YesHandler = {
    canHandle(handlerInput) {
        console.log('inside yes can handle');
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const sessionAttributes = attributesManager.getSessionAttributes();

        return request.type === 'IntentRequest' && (request.intent.name === 'AMAZON.YesIntent' || sessionAttributes.states === states.HELP);
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;

        const sessionAttributes = attributesManager.getSessionAttributes();
        sessionAttributes.states = states.YES;
        var speechOutput = `Well then. Which football club do you support ?. `;
        var reprompt = 'Say, the club name. For instance, i support manchester united. '
        sessionAttributes.speechOutput = speechOutput;
        sessionAttributes.reprompt = reprompt
        return responseBuilder.speak(speechOutput).reprompt(reprompt).getResponse();
    },
};

const QuizHandler = {

    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        console.log("inside quiz handle");

        return request.type === 'IntentRequest' && request.intent.name === 'QuizIntent';
    },
    handle(handlerInput) {
        console.log("inside handler quiz input");
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;
        var speechOutput;
        console.log('inside quizIntent');
        console.log('get attributes');
        const sessionAttributes = attributesManager.getSessionAttributes();
        sessionAttributes.states = states.QUIZ;
        sessionAttributes.counter = 1;
        console.log('attributes set');



        for (var key in quiz) {
            console.log('inside for loop');
            const request = handlerInput.requestEnvelope.request;
            if (request.intent.slots.teamname.resolutions && request.intent.slots.teamname.resolutions.resolutionsPerAuthority[0]) {
                if (request.intent.slots.teamname.resolutions.resolutionsPerAuthority[0].status.code == 'ER_SUCCESS_MATCH') {
                    var a = request.intent.slots.teamname.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(key)
                    var d = key.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        console.log('inisde equals')
                        speechOutput = `Let'\s start the rapid fire. Here comes your first question. ${quiz[key]['Question 1']['Q']} The options are option a,  ${quiz[key]['Question 1']['A']}, <break time="0.25s"/> option b, ${quiz[key]['Question 1']['B']}, <break time="0.25s"/> option c, ${quiz[key]['Question 1']['C']}, <break time="0.25s"/> option d, ${quiz[key]['Question 1']['D']}. Choose the right option. `;
                        var reprompt = `You may answer as option a or option b or option of your choice. `;
                        sessionAttributes.key = key;
                        console.log(`${sessionAttributes.key} and ${sessionAttributes.counter}`);
                        break;

                    } else {
                        speechOutput = `Sorry. I did not get you. Can you please repeat it. `
                        var reprompt = `You may say Barcelona. `

                    }
                } else {
                    speechOutput = `Sorry. I did not get you. Can you please repeat it. `
                    var reprompt = `You may say Barcelona. `

                }
            } else {
                speechOutput = `Sorry. I did not get you. Can you please repeat it. `
                var reprompt = `You may say Barcelona. `

            }

        }
        sessionAttributes.speechOutput = speechOutput;
        sessionAttributes.reprompt = reprompt;
        return responseBuilder.speak(speechOutput).reprompt(reprompt).getResponse();
    }
};

const AnswerHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest' && request.intent.name === 'AnswerIntent';
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;

        const sessionAttributes = attributesManager.getSessionAttributes();
        sessionAttributes.states = states.ANSWER;
        var speechOutput;
        if (request.intent.slots.answer.resolutions && request.intent.slots.answer.resolutions.resolutionsPerAuthority[0]) {
            if (request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].status.code == 'ER_SUCCESS_MATCH') {
                if (sessionAttributes.counter === 1) {
                    console.log('inside 1');
                    sessionAttributes.right = 0;
                    sessionAttributes.wrong = 0;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 1']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key)
                    console.log(value)
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    sessionAttributes.counter = 2;
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `Here is the second question. ${quiz[`${sessionAttributes.key}`]['Question 2']['Q']} The options are option a,   ${quiz[sessionAttributes.key]['Question 2']['A']},  <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 2']['B']},  <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 2']['C']},  <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 2']['D']}. Choose the right option. `
                        sessionAttributes.right = 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `Here is the second question. ${quiz[`${sessionAttributes.key}`]['Question 2']['Q']} The options are option a,   ${quiz[sessionAttributes.key]['Question 2']['A']},  <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 2']['B']},  <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 2']['C']},  <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 2']['D']}. Choose the right option. `
                        sessionAttributes.wrong = 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }
                } else if (sessionAttributes.counter === 2) {
                    console.log('inside two')
                    sessionAttributes.counter = 3;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 2']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id;
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key)
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `Here comes your third question. ${quiz[`${sessionAttributes.key}`]['Question 3']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 3']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 3']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 3']['C']},  <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 3']['D']}. Choose the right option. `
                        sessionAttributes.right += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `Here comes your third question. ${quiz[`${sessionAttributes.key}`]['Question 3']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 3']['A']}, <break time="0.25s"/>  option b, ${quiz[sessionAttributes.key]['Question 3']['B']}, <break time="0.25s"/>  option c, ${quiz[sessionAttributes.key]['Question 3']['C']},  <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 3']['D']}. Choose the right option. `
                        sessionAttributes.wrong += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }



                } else if (sessionAttributes.counter === 3) {
                    sessionAttributes.counter = 4;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 3']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key);
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `Here is the fourth question. ${quiz[`${sessionAttributes.key}`]['Question 4']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 4']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 4']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 4']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 4']['D']}. Choose the right option. `
                        sessionAttributes.right += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `Here is the fourth question. ${quiz[`${sessionAttributes.key}`]['Question 4']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 4']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 4']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 4']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 4']['D']}. Choose the right option. `
                        sessionAttributes.wrong += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }



                } else if (sessionAttributes.counter === 4) {
                    sessionAttributes.counter = 5;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 4']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key)
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `You are half way through. Here is your fifth question.  ${quiz[`${sessionAttributes.key}`]['Question 5']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 5']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 5']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 5']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 5']['D']}. Choose the right option. `
                        sessionAttributes.right += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `You are half way through. Here is your fifth question.  ${quiz[`${sessionAttributes.key}`]['Question 5']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 5']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 5']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 5']['C']} <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 5']['D']}. Choose the right option. `
                        sessionAttributes.wrong += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }



                } else if (sessionAttributes.counter === 5) {
                    sessionAttributes.counter = 6;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 5']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key)
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `Here is the sixth question. ${quiz[`${sessionAttributes.key}`]['Question 6']['Q']}   The options are option a,  ${quiz[sessionAttributes.key]['Question 6']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 6']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 6']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 6']['D']}. Choose the right option. `
                        sessionAttributes.right += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `Here is the sixth question. ${quiz[`${sessionAttributes.key}`]['Question 6']['Q']}   The options are option a,  ${quiz[sessionAttributes.key]['Question 6']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 6']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 6']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 6']['D']}. Choose the right option. `
                        sessionAttributes.wrong += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }



                } else if (sessionAttributes.counter === 6) {
                    sessionAttributes.counter = 7;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 6']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key)
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `Gear up for the seventh question. Here it comes. ${quiz[`${sessionAttributes.key}`]['Question 7']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 7']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 7']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 7']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 7']['D']}. Choose the right option. `
                        sessionAttributes.right += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `Gear up for the  seventh question. Here it comes.  ${quiz[`${sessionAttributes.key}`]['Question 7']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 7']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 7']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 7']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 7']['D']}. Choose the right option. `
                        sessionAttributes.wrong += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }



                } else if (sessionAttributes.counter === 7) {
                    sessionAttributes.counter = 8;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 7']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key);
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `Coming to the eight question. ${quiz[`${sessionAttributes.key}`]['Question 8']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 8']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 8']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 8']['C']} <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 8']['D']}. Choose the right option. `
                        sessionAttributes.right += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `coming to the eight question. ${quiz[`${sessionAttributes.key}`]['Question 8']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 8']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 8']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 8']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 8']['D']}. Choose the right option. `
                        sessionAttributes.wrong += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }



                } else if (sessionAttributes.counter === 8) {
                    sessionAttributes.counter = 9;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 8']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key)
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `Here is the ninth question. ${quiz[`${sessionAttributes.key}`]['Question 9']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 9']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 9']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 9']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 9']['D']}. Choose the right option. `
                        sessionAttributes.right += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `Here is the ninth question. ${quiz[`${sessionAttributes.key}`]['Question 9']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 9']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 9']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 9']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 9']['D']}. Choose the right option. `
                        sessionAttributes.wrong += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }



                } else if (sessionAttributes.counter === 9) {
                    sessionAttributes.counter = 10;
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 9']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key)
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `Here is the final question. ${quiz[`${sessionAttributes.key}`]['Question 10']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 10']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 10']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 10']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 10']['D']}. Choose the right option. `
                        sessionAttributes.right += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';

                    } else {
                        speechOutput = `Here is the final question. ${quiz[`${sessionAttributes.key}`]['Question 10']['Q']} The options are option a,  ${quiz[sessionAttributes.key]['Question 10']['A']}, <break time="0.25s"/> option b, ${quiz[sessionAttributes.key]['Question 10']['B']}, <break time="0.25s"/> option c, ${quiz[sessionAttributes.key]['Question 10']['C']}, <break time="0.25s"/> option d, ${quiz[sessionAttributes.key]['Question 10']['D']}. Choose the right option. `
                        sessionAttributes.wrong += 1;
                        var reprompt = 'You may answer as option a or option b or option of your choice. ';
                    }



                } else if (sessionAttributes.counter === 10) {
                    var value = `${quiz[`${sessionAttributes.key}`]['Question 10']['answer']}`;
                    var a = request.intent.slots.answer.resolutions.resolutionsPerAuthority[0].values[0].value.id
                    console.log(a);
                    var t = a.replace(/\s/g, '');
                    console.log(sessionAttributes.key)
                    console.log(value);
                    var d = value.replace(/\s/g, '');
                    console.log(d);
                    if (t.toLowerCase() == d.toLowerCase()) {
                        speechOutput = `How many do you think you answered right ?.  Go ahead. Have a guess.`;
                        sessionAttributes.right += 1;
                    } else {
                        speechOutput = `How many do you think you answered right ?.  Go ahead. Have a guess.`;
                        sessionAttributes.wrong += 1;
                    }
                }

            } else {
                speechOutput = `Sorry I did not get you. Can you please repeat it. `
                reprompt = 'You may answer as option a or option b or option of your choice.'


            }
        } else {
            speechOutput = `Sorry I did not get you. Can you please repeat it. `
            reprompt = 'You may answer as option a or option b or option of your choice.'


        }

        sessionAttributes.speechOutput = speechOutput;
        sessionAttributes.reprompt = reprompt;

        return responseBuilder.speak(speechOutput).reprompt('You may answer as option a or option b or option of your choice.').getResponse();

    }

};

const EndHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest' && request.intent.name === 'EndIntent';
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;

        const sessionAttributes = attributesManager.getSessionAttributes();
        sessionAttributes.states = states.END;
        var speechOutput;
        console.log(request.intent.slots.number.value);
        var q = request.intent.slots.number.value
        console.log(q);
        if (q == sessionAttributes.right) {
            if (sessionAttributes.right < 5) {

                speechOutput = `You seem to know yourself quite a bit. But, you need to start digging though. You were able to answer only ${sessionAttributes.right} correctly.  If you think you can do better again,  Say play again. If you want to tweet this score , say tweet to challenge your peers. To exit, say stop.  `
                var reprompt = 'Say play again, else say stop to exit. ';

            } else if (5 < sessionAttributes.right < 8) {
                speechOutput = ` You are right, you were able to answer ${sessionAttributes.right} questions correctly. I think you can do better. Say, play again to give it a go. If you want to tweet this score and challenge your friends , say tweet. To exit, say stop.  `
                var reprompt = 'Say play again, else say stop to exit.';

            } else if (sessionAttributes.right > 8) {
                speechOutput = `Hello true fanatic. You are absolutely right. You answered ${sessionAttributes.right} questions correctly. But are you a true football fanatic. Try out other clubs and see if you can sweep us off. Say play again. If you want to tweet this score and challenge your friends , say tweet. To exit say stop.  `
                var reprompt = 'Say play again, else say stop to exit. ';
            }

        } else {
            speechOutput = `Oops. You are wrong. You have answered ${sessionAttributes.right} questions correctly. To take the quiz of any other team.  Say Play again. If you want to tweet this score and challenge your friends , say tweet. Else say stop. `
            var reprompt = 'You may say any other team name';
        }
        sessionAttributes.speechOutput = speechOutput;
        sessionAttributes.reprompt = reprompt;
        return responseBuilder.speak(speechOutput).reprompt(reprompt).getResponse();
    },
};

const AgainHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest' && request.intent.name === 'AgainIntent';
    },
    handle(handlerInput) {

        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;

        const sessionAttributes = attributesManager.getSessionAttributes();
        sessionAttributes.states = states.AGAIN;
        var speechOutput;
        delete sessionAttributes.counter;
        delete sessionAttributes.right;

        speechOutput = `Please pick a team of your choice to start the quiz. Teams available are Chelsea, Arsenal, Manchester United, Real Madrid, Barcelona, Liverpool, Borussia Dortmund, AC Milan, Inter Milan, juventus and bayern Munich. Please pick any of them. `
        sessionAttributes.speechOutput = speechOutput;
        var reprompt = 'Teams available are Chelsea, Arsenal, Manchester United, Real Madrid, Barcelona, Liverpool, Borussia Dortmund, AC Milan, Inter Milan, juventus and bayern Munich. Please pick any of them. '
        sessionAttributes.reprompt = reprompt;


        return responseBuilder.speak(speechOutput).reprompt(reprompt).getResponse();
    }


};

const RepeatHandler = {
    canHandle(handlerInput) {
        console.log('inside repeat can handle');
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const sessionAttributes = attributesManager.getSessionAttributes();

        return request.type === 'IntentRequest' && request.intent.name === 'RepeatIntent';
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;

        const sessionAttributes = attributesManager.getSessionAttributes();
        sessionAttributes.states = states.REPEAT;

        return responseBuilder.speak(sessionAttributes.speechOutput).reprompt(sessionAttributes.reprompt).getResponse();
    },

};


const TweetHandler = {

    canHandle(handlerInput) {
        console.log('inside tweet can handle');
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const sessionAttributes = attributesManager.getSessionAttributes();

        return request.type === 'IntentRequest' && request.intent.name === 'TweetIntent';
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;

        const sessionAttributes = attributesManager.getSessionAttributes();
        if (!handlerInput.requestEnvelope.session.user.accessToken || handlerInput.requestEnvelope.session.user.accessToken == null || handlerInput.requestEnvelope.session.user.accessToken == undefined ||handlerInput.requestEnvelope.session.user.accessToken === "") {
            speechOutput = `Please check your companion app to link your twitter account. After linking up say, tweet to post your score. <break time="0.25s"/> Else say exit.`
            var reprompt = `Say, yes to continue. `
            
            return responseBuilder.speak(speechOutput).reprompt(reprompt).withLinkAccountCard().getResponse();
                

        } else {
            var speechOutput = `Your tweet has been sent!.  To play again say play again. <break time="0.25s"/> Else say stop to exit.`;
            var reprompt = 'Say play again to play. Else say stop to exit. '
            var twitter = new TwitterHelper(handlerInput.requestEnvelope.session.user.accessToken);
            var feed = `I have just scored ${sessionAttributes.right} fanatic points for my club ${sessionAttributes.key}. Do you think you can beat me ?. Go check out the football fanatics skill in the alexa skills store.  #FootballFanatics` ;
            console.log('tweeting');
            twitter.postTweet(feed) ;
            console.log('inside tweeting')
            return responseBuilder.speak(speechOutput).reprompt(reprompt).getResponse();
               

        }
    },


};



const ExitHandler = {
    canHandle(handlerInput) {
        console.log("Inside ExitHandler");
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        const request = handlerInput.requestEnvelope.request;


        return request.type === `IntentRequest` && (
            request.intent.name === 'AMAZON.StopIntent' ||
            request.intent.name === 'AMAZON.NoIntent' ||
            request.intent.name === 'AMAZON.CancelIntent'
        );
    },
    handle(handlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        var feed = `I have just scored ${sessionAttributes.right} fanatic points for my club ${sessionAttributes.key}. Do you think you can beat me ?. Go check out the football fanatics skill in the alexa skills store.  #FootballFanatics` ;
        if(handlerInput.requestEnvelope.session.user.accessToken){
        var twitter = new TwitterHelper(handlerInput.requestEnvelope.session.user.accessToken);
        twitter.postTweet(feed) ;
    }
        if(sessionAttributes.right =! undefined){
        return handlerInput.responseBuilder
            .speak(`Thanks for trying out the football fanatics. Do spread the word and challenge your friends to beat your score of ${sessionAttributes.right}.   Bye!`)
            .withShouldEndSession(true)
            .getResponse();
        }
        else{
            return handlerInput.responseBuilder
            .speak(`Thanks for your valuable time. Hope to see you again. Bye!`)
            .withShouldEndSession(true)
            .getResponse();

        }
    },
};

const HelpHandler = {
    canHandle(handlerInput) {
        console.log("Inside HelpHandler");
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' &&
            request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        const attributesManager = handlerInput.attributesManager;
        const responseBuilder = handlerInput.responseBuilder;
        console.log('Help Intent')
        const sessionAttributes = attributesManager.getSessionAttributes();
        sessionAttributes.states = states.HELP;
        console.log('attributes set');
        var speechOutput = `How well do you think you know your club ?. Come participate in this engaging rapid fire round. You will be presented with 10 multiple choice questions. Answer them and see how many of them you could get right. You can also post your score on twitter and challenge your fellow peers. Say, let'\s play to begin the game.`
        var reprompt = `Say, let\'s play to begin.  `
        sessionAttributes.speechOutput = speechOutput;
        sessionAttributes.reprompt = reprompt;

        console.log("Inside HelpHandler - handle");
        return handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(reprompt)
            .getResponse();
    },
};

const SessionEndedHandler = {
    canHandle(handlerInput) {
        console.log("Inside SessionEndedRequestHandler");
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`Session ended with reason: ${JSON.stringify(handlerInput.requestEnvelope)}`);
        return handlerInput.responseBuilder.speak('Bye!').getResponse();
    },
};



const FallbackHandler = {
    canHandle(handlerInput) {
        console.log("Inside FallbackHandler");
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' &&
            request.intent.name === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        console.log("Inside FallbackHandler - handle");
        return handlerInput.responseBuilder
            .speak(`Sorry, i did not get you. Can you please repeat it.`)
            .reprompt(`Sorry, i did not get you. Can you please repeat it.`)
            .getResponse();
    },

};

const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`Error handled: ${error.message}`);

        return handlerInput.responseBuilder
            .speak('Seems like there is some error. We will be back shortly. Thank You.')
            .getResponse();
    },
};



