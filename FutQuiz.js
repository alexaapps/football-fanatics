module.exports = {
    'Manchester United' : {
        'Question 1' : {
            'Q' : 'How many Premier League titles have the club won ?',
            'A' : '13',
            'B' : '10',
            'C' : '20',
            'D' : '15',
            'answer' : 'option A'
       },
        'Question 2' : {
            'Q' : 'Joel Glazer is one of the clubs chairmen but what is the name of the other ?',
            'A' : 'Jim Glazer',
            'B' : 'David Gill',
            'C' : 'Avram Glazer',
            'D' : 'Micheal Knighton',
            'answer' : 'option C'

        },
        'Question 3' : {
            'Q' : 'What was David Moyes doing when Alex Ferguson rang him to ask him to be United manager?',
            'A' : 'Out clubbing with Marouane Fellaini',
            'B' : 'Watching Breaking Bad',
            'C' : 'Negotiating a new contract at everton',
            'D' : 'Out shopping with his wife',
            'answer' : 'option D'
        },
        'Question 4' : {
            'Q' : ' Complete the following sentence, Former United captain Roy Keane famously ridiculed the section of the clubs support who ',
            'A' : 'Refused to chant his name',
            'B' : 'Booed him whenever he touched the ball',
            'C' : 'Ate prawn sandwiches',
            'D' : 'Refused to buy his autobiography',
            'answer' : 'option C'

        },
        'Question 5' : {
            'Q' : ' What was the clubs name before it became Manchester United ?',
            'A' : 'Newton Heath',
            'B' : 'FC United',
            'C' : 'The Royal Engineers',
            'D' : 'Manchester City',
            'answer' : 'option A'

        },
        'Question 6' : {
            'Q' : ' Man United won their first ever First Division league title in',
            'A' : '1902',
            'B' : '1908',
            'C' : '1922',
            'D' : '1927',
            'answer' : 'option B'
        },
        'Question 7' : {
            'Q' : ' Against, Whom did treble-winner Jesper Blomqvist score his only Manchester United goal against ?',
            'A' : 'Tottenham',
            'B' : 'Aston Villa',
            'C' : 'Everton',
            'D' : 'Manchester City',
            'answer' : 'option C'
        },
        'Question 8' : {
            'Q' : ' What did the Man United scout who watched George Best at the age of 15 famously tell Matt Busby ?',
            'A' : 'Best by name, best by nature',
            'B' : 'This lad will never make it',
            'C' : 'I think i have found you a genius',
            'D' : 'I think i have just seen the future',
            'answer' : 'option C'
        },
        'Question 9' : {
            'Q' : ' In a January 2011 worldwide poll conducted by Man Uniteds official magazine and website, their best ever player was named as ',
            'A' : 'Denis Law',
            'B' : 'Eric Cantona',
            'C' : 'Bobby Charlton',
            'D' : 'Ryan Giggs',
            'answer' : 'option D'
        },
        'Question 10' : {
            'Q' : ' "After his first training session in heaven, George Best, from the favourite right wing, turned the head of God who was filling in at left back." Who said this ?',
            'A' : 'Denis Irwin',
            'B' : 'Eric Cantona',
            'C' : 'Gary Neville',
            'D' : 'Ryan Giggs',
            'answer' : 'option B'
        },



  },
    
    'Real Madrid' : {
        'Question 1' : {
            'Q' : 'In what year were Real Madrid formed ?',
            'A' : '1905',
            'B' : '1902',
            'C' : '1910',
            'D' : '1906',
            'answer' : 'option B'
       },
        'Question 2' : {
            'Q' : 'From which Colombian team did they sign Alberto Di Stefano ?',
            'A' : 'Millonarios',
            'B' : ' Deportino Cali',
            'C' : 'Deportes Tomila',
            'D' : 'Sevilla FC',
            'answer' : ' option A'

        },
        'Question 3' : {
            'Q' : 'Who did they famously beat 7-3 in the 1960 European Cup final ?',
            'A' : 'Eintracht Frankfurt',
            'B' : 'Barcelona',
            'C' : 'Nice',
            'D' : 'Liverpool',
            'answer' : ' option A'
        },
        'Question 4' : {
            'Q' : ' Who is the clubs longest serving manager ? ',
            'A' : 'Francisco Bru',
            'B' : 'Ramón Encinas',
            'C' : 'Miguel Munoz',
            'D' : 'Arthur Jhonson',
            'answer' : ' option C'

        },
        'Question 5' : {
            'Q' : 'Who was the club president with the galactico dream ',
            'A' : 'Lorenzo Sanz',
            'B' : 'Florentino Perez',
            'C' : ' Fernando Martín Álvarez',
            'D' : 'Ramón Calderón',
            'answer' : 'option B'
        },
        'Question 6' : {
            'Q' : ' Who was the first galactico signing ?',
            'A' : 'Zinedine Zidane',
            'B' : 'Luis Figo',
            'C' : 'David Beckham',
            'D' : 'Kaka',
            'answer' : 'option B'
        },
        'Question 7' : {
            'Q' : ' How many Liga titles did David Beckham win in his time in Real Madrid ',
            'A' : 'One',
            'B' : 'Three',
            'C' : 'Two',
            'D' : 'Four',
            'answer' : 'option A'
        },
        'Question 8' : {
            'Q' : ' How much did Cristiano Ronaldo cost when he signed from Manchester United ?',
            'A' : '90 million pounds',
            'B' : '85 million pounds',
            'C' : '80 million pounds',
            'D' : '89.5 million pounds',
            'answer' : 'option C'
        },
        'Question 9' : {
            'Q' : ' Which team did Real Madrid beat to land La Decima ? ',
            'A' : 'Juventus',
            'B' : 'Atletico Madrid',
            'C' : 'Bayern Munich',
            'D' : 'Milan',
            'answer' : 'option B'
        },
        'Question 10' : {
            'Q' : ' "Once the Real Madrids leading goal-scorer of all time was Raúl González. How many goals did he score in total ?',
            'A' : '327',
            'B' : '323',
            'C' : '357',
            'D' : '298',
            'answer' : 'option B'
        },

    },

    'Barcelona' : {
        'Question 1' : {
            'Q' : ' When was FC Barcelona founded ? ',
            'A' : 'November 11 ,1898',
            'B' : 'November 29, 1899',
            'C' : 'December 1, 1900',
            'D' : 'October 13, 1901',
            'answer' : 'option B'
       },
        'Question 2' : {
            'Q' : 'How many Copa Del Rey has FC Barcelona won ?',
            'A' : '31',
            'B' : '32',
            'C' : '29',
            'D' : '30',
            'answer' : 'option C'

        },
        'Question 3' : {
            'Q' : 'Since what year has Barca used The Nou Camp as their stadium ?',
            'A' : '1946',
            'B' : '1957',
            'C' : '1960',
            'D' : '1952',
            'answer' : 'option B'
        },
        'Question 4' : {
            'Q' : ' In what stadium did Barca play before they moved to the Nou Camp ?',
            'A' : 'Las Corts',
            'B' : 'Estado Olímpic Lluís',
            'C' : 'Mestalla',
            'D' : 'Martínez Valero',
            'answer' : 'option A'

        },
        'Question 5' : {
            'Q' : 'How many goals did Ronaldo of brazil score for Barcelona ?',
            'A' : '41',
            'B' : '54',
            'C' : '60',
            'D' : '38',
            'answer' : 'option B'
        },
        'Question 6' : {
            'Q' : 'One of Barcas best player of all time, Rivaldo, started his career for a small club in Brazil called' ,
            'A' : 'Mogi Mirim',
            'B' : 'Santa Cruz',
            'C' : 'Corinthians',
            'D' : 'Sao Paulo',
            'answer' : 'option B'
        },
        'Question 7' : {
            'Q' : ' What was Barcas biggest win against Real Madrid in the 20th Century?',
            'A' : ' five one',
            'B' : 'Five nil',
            'C' : 'Six one',
            'D' : 'four nil',
            'answer' : 'option B'
        },
        'Question 8' : {
            'Q' : 'Messi, Suarez and Neymar scored how many goals for Barca in 2015 ?',
            'A' : '105',
            'B' : '127',
            'C' : '137',
            'D' : '115',
            'answer' : 'option B'
        },
        'Question 9' : {
            'Q' : 'Who did Barca beat in their first European Cup final back in 1992 ? ',
            'A' : 'Milan',
            'B' : 'Ajax',
            'C' : 'Sampodaria',
            'D' : 'Juventus',
            'answer' : 'option C'
        },
        'Question 10' : {
            'Q' : ' "How many individual Barca players have won the Pichichi Trophy ?',
            'A' : '5',
            'B' : '6',
            'C' : '10',
            'D' : '7',
            'answer' : 'option C'
        },


    },

    'Chelsea' : {

        'Question 1' : {
            'Q' : ' In 1998 who scored the winning goal in Chelseas Cup Winners Cup victory ?',
            'A' : 'Gus Poyet',
            'B' : 'Dennis Wise',
            'C' : 'Gianfranco Zola',
            'D' : 'Tore Andre Flo',
            'answer' : 'option C'
       },
        'Question 2' : {
            'Q' : 'How many times have Chelsea won the FA Cup ?',
            'A' : '4',
            'B' : '7',
            'C' : '9',
            'D' : '8',
            'answer' : 'option B'

        },
        'Question 3' : {
            'Q' : 'In what year did Chelsea win their first FA Charity cup, also known as Community Shield ?',
            'A' : '1949',
            'B' : '1951',
            'C' : '1963',
            'D' : '1955',
            'answer' : 'option D'
        },
        'Question 4' : {
            'Q' : ' What is Frank Lampards highest goal tally for any one season ?',
            'A' : '22',
            'B' : '19',
            'C' : '16',
            'D' : '31',
            'answer' : 'option A'

        },
        'Question 5' : {
            'Q' : 'Who did Frank Lampard overtake as Chelseas all-time top goal scorer ?',
            'A' : 'Didier Drogba',
            'B' : 'Kerry Dixon',
            'C' : 'Peter Osgood',
            'D' : 'Bobby Tambling',
            'answer' : 'option D'
        },
        'Question 6' : {
            'Q' : 'What is the full capacity of Stamford Bridge ?' ,
            'A' : 'Sixty three thousand one forty two',
            'B' : 'Thirty eight thousand one sixty five',
            'C' : 'Fifty Five thousand',
            'D' : 'Forty one thousand eight thirty seven',
            'answer' : 'option D'
        },
        'Question 7' : {
            'Q' : 'Between Jose Mourinhos departure as Chelsea manager in 2007 and his return in 2013, how many managers did Chelsea have ?',
            'A' : '6',
            'B' : '3',
            'C' : '13',
            'D' : '8',
            'answer' : 'option D'
        },
        'Question 8' : {
            'Q' : 'In the 2010-11 season, what was the colour of Chelseas away kit ?',
            'A' : 'Orange',
            'B' : 'Green',
            'C' : 'White',
            'D' : 'Black',
            'answer' : 'option B'
        },
        'Question 9' : {
            'Q' : 'What is the name of Chelseas official mascot ?',
            'A' : 'The Blue Bear',
            'B' : 'Stamford Lion',
            'C' : 'The Chelsea Cheater',
            'D' : 'The Fulham road flamingo',
            'answer' : 'option C'
        },
        'Question 10' : {
            'Q' : ' What number did the former Chelsea player and manager Roberto Di Matteo wear on his shirt ?',
            'A' : '15',
            'B' : '16',
            'C' : '11',
            'D' : '18',
            'answer' : 'option B'
        },

    },

    'Arsenal' : {
        
        'Question 1' : {
            'Q' : 'Who was Arsenal’s first ever manager ?',
            'A' : 'Herbert Chapman',
            'B' : 'Arsene Wenger',
            'C' : 'William Elcoat',
            'D' : 'Thomas Mitchell',
            'answer' : 'option D'
       },
        'Question 2' : {
            'Q' : 'Whose record did Ian Wright beat to become the highest ever goalscorer ?',
            'A' : 'Cliff Bastin',
            'B' : 'Alex James',
            'C' : 'Ted Drake',
            'D' : 'Dennis Bergkamp',
            'answer' : 'option A'

        },
        'Question 3' : {
            'Q' : 'Which player has scored the most goals in the North London derby ?',
            'A' : 'Thierry Henry', 
            'B' : 'Dennis Bergkamp',
            'C' : 'Emmanuel Adebayor',
            'D' : 'Ian Wright',
            'answer' : 'option C'
        },
        'Question 4' : {
            'Q' : 'Who did Arsenal beat for their first Premier League win at Highbury ?',
            'A' : 'Norwich City',
            'B' : 'Sunderland',
            'C' : 'Sheffield Wednesday',
            'D' : 'Oldham Atletic',
            'answer' : 'option D'

        },
        'Question 5' : {
            'Q' : 'Who scored the first competitive goal at the Emirates Stadium ?',
            'A' : 'Olof Mellberg',
            'B' : 'Gilberto Silva',
            'C' : 'J\'lloyd Samuel',
            'D' : 'PAt Jennings',
            'answer' : 'option A'
        },
        'Question 6' : {
            'Q' : 'How old was Arsene Wenger when he took over at Arsenal ?' ,
            'A' : '45',
            'B' : '46',
            'C' : '47',
            'D' : '44',
            'answer' : 'option C'
        },
        'Question 7' : {
            'Q' : 'Which teams scored against Arsenal on their way to the 2005/06 CL final?',
            'A' : 'Real madrid and Ajax',
            'B' : 'Ajax and FC Thun',
            'C' : 'Sparta Prague and Real Madrid',
            'D' : 'Real Madrid and FC Thun',
            'answer' : 'optionB'
        },
        'Question 8' : {
            'Q' : 'Against Whom did Thierry Henry score his last Arsenal goal against?',
            'A' : 'Sunderland',
            'B' : 'Blackburn Rovers',
            'C' : 'Bolton Wanderes',
            'D' : 'Norwich City',
            'answer' : ' Option A'
        },
        'Question 9' : {
            'Q' : 'Name the first player to play for Wenger, born during his reign ?',
            'A' : 'Benik Afobe',
            'B' : 'Chuba Akpom',
            'C' : 'Gideon Zealalem',
            'D' : 'Walcott',
            'answer' : ' Option C'
        },
        'Question 10' : {
            'Q' : 'Who was the first player to wear the No6 shirt after Tony Adams retired ?',
            'A' : 'Philippe Senderos',
            'B' : 'Laurent Koscielny',
            'C' : 'Kolo Toure',
            'D' : 'Martin Keown',
            'answer' : ' Option A'
        },
    },

    'liverpool' : {
        'Question 1' : {
            'Q' : 'Robbie Fowler held the Premier League record for the fastest hat-trick before it was broken in 2015. How quickly did Fowler do it in ?',
            'A' : '3 minutes 54 seconds',
            'B' : '4 minutes 33 seconds ',
            'C' : 'Five minutes 42 seconds',
            'D' : '4 minutes 20 seconds',
            'answer' : 'option B'
       },
        'Question 2' : {
            'Q' : 'Who assisted Vladimír Šmicer for his goal against AC Milan in the 2005 Champions League final ?',
            'A' : 'Milan Baros',
            'B' : 'Luis Garcia',
            'C' : 'Steven Gerrard',
            'D' : 'Dietmar Hamman',
            'answer' : 'option D'

        },
        'Question 3' : {
            'Q' : 'Which of these Liverpool flops made the fewest appearances for the Reds ?',
            'A' : 'Iago Aspas', 
            'B' : 'Lazar Markovic',
            'C' : 'Paul Konchesky',
            'D' : 'Milan Jovanovic',
            'answer' : 'option A'
        },
        'Question 4' : {
            'Q' : 'Who was Steven Gerrard\'s first goal for Liverpool against?',
            'A' : 'Norwich City',
            'B' : 'Sunderland',
            'C' : 'Sheffield Wednesday',
            'D' : 'Newcastle United',
            'answer' : 'option C'

        },
        'Question 5' : {
            'Q' : 'Bought in 2001 for around 5 million pounds, I made 186 appearances for the Reds and was Man of the Match in the win against Manchester United in the 2003 League Cup final. Who am I ?',
            'A' : 'Jerzy Dudek ',
            'B' : 'El Hadji Diouf',
            'C' : 'John Arne Riise',
            'D' : ' Stéphane Henchoz',
            'answer' : 'option A'
        },
        'Question 6' : {
            'Q' : 'Who was in Liverpool\'s group when they won the Champions League in 2005 ?' ,
            'A' : 'Lille',
            'B' : 'Monaco',
            'C' : 'Bayer Leverkusen',
            'D' : 'Dynamo Kyiv',
            'answer' : 'option B'
        },
        'Question 7' : {
            'Q' : 'Who did Liverpool beat in the semi finals en route to winning their first European Cup in 1977 ?',
            'A' : 'Trabzonspor',
            'B' : 'FC Zurich',
            'C' : 'Manchester United',
            'D' : 'Barcelona',
            'answer' : 'option B'
        },
        'Question 8' : {
            'Q' : 'Who were the first side to win a european game in anfield ?',
            'A' : 'Juventus',
            'B' : 'Internazionale',
            'C' : 'Ferencvaros',
            'D' : 'Barcelona',
            'answer' : ' Option C'
        },
        'Question 9' : {
            'Q' : 'Excluding the Super Cup, how many European trophies have Liverpool won ?',
            'A' : 'Six',
            'B' : 'Seven',
            'C' : 'Eight',
            'D' : 'Nine',
            'answer' : ' Option C'
        },
        'Question 10' : {
            'Q' : 'When did Liverpool reach their first European final ?',
            'A' : '1966',
            'B' : '1969',
            'C' : '1973',
            'D' : '1977',
            'answer' : ' Option A'
        },

    },

    'Juventus' : {

        'Question 1' : {
            'Q' : 'Before Juventus Buffon played in ',
            'A' : 'FC Paleramo',
            'B' : 'FC Parma ',
            'C' : 'SS Lazio',
            'D' : 'AS Roma',
            'answer' : 'option B'
       },
        'Question 2' : {
            'Q' : 'Who has been the most successful Juventus Manager of all time ?',
            'A' : 'Dino Zoff',
            'B' : 'Giovani Trapatoni',
            'C' : 'Antonio Conte',
            'D' : 'Carlo Ancelotti',
            'answer' : 'option B'

        },
        'Question 3' : {
            'Q' : 'Juventus signed Antonio Conte from who in 1991 ?',
            'A' : 'Reggiana', 
            'B' : 'Cremonese',
            'C' : 'Pisa',
            'D' : 'Lecce',
            'answer' : 'option D'
        },
        'Question 4' : {
            'Q' : 'Who scored Juventus\'s goal in the 1996 Champions League final?',
            'A' : 'Jugovic',
            'B' : 'Del Piero',
            'C' : 'Ravenelli',
            'D' : 'Vialli',
            'answer' : 'Option C'

        },
        'Question 5' : {
            'Q' : 'Which player captained Juventus in the 1997 Champions League final?',
            'A' : 'Di Livio',
            'B' : 'Deschamps',
            'C' : 'Peruzzi',
            'D' : 'Boksic',
            'answer' : 'option C'
        },
        'Question 6' : {
            'Q' : 'When was Juventus Founded ? ' ,
            'A' : '1890',
            'B' : '1897',
            'C' : '1907',
            'D' : '1920',
            'answer' : 'option B'
        },
        'Question 7' : {
            'Q' : 'These stars stayed with Juventus when Juve relegated to Serie-B after Calciopoli, except',
            'A' : 'Gianluigi Buffon',
            'B' : 'David Trezeguet',
            'C' : 'Alessandro Del Piero',
            'D' : 'Gianluga Zambrotta',
            'answer' : 'option D'
        },
        'Question 8' : {
            'Q' : 'What colour was the Juventus kit before the black and white stripes kit ?',
            'A' : 'Red',
            'B' : 'Orange and White',
            'C' : 'Blue and Black',
            'D' : 'Pink',
            'answer' : ' Option D'
        },
        'Question 9' : {
            'Q' : 'Which among these is a juventus mascot ?',
            'A' : 'J  the zebra',
            'B' : 'Red Devil',
            'C' : 'Rhino',
            'D' : 'The Old Lady',
            'answer' : ' Option A'
        },
        'Question 10' : {
            'Q' : 'Turin Derby is also known as ',
            'A' : 'Derby della Mole',
            'B' : 'Blue Moon Derby',
            'C' : 'EL clasico',
            'D' : 'Derby of Italy',
            'answer' : ' Option A'
        },


    },

    'Bayern Munich' : {

        'Question 1' : {
            'Q' : 'Who did Manchester United sign from Bayern Munich in 2015 ? ',
            'A' : 'Lewandowski',
            'B' : 'Jerome Boateng ',
            'C' : 'Bastian Schweinsteiger',
            'D' : 'Mario Goetze',
            'answer' : 'Opiton C'
       },
        'Question 2' : {
            'Q' : 'Between two spells with Bayern Munich, Lothar Matthäus played for which Italian side between 1988 and 1992 ?',
            'A' : 'Sampdoria',
            'B' : 'Juventus',
            'C' : 'Fiorentina',
            'D' : 'Internazionale',
            'answer' : 'option D'

        },
        'Question 3' : {
            'Q' : 'Who was the only team that Bayern Munich did the double over in the 2011/12 Champions League?',
            'A' : 'Napoli', 
            'B' : 'Manchester City',
            'C' : 'Villareal',
            'D' : 'Lille',
            'answer' : 'option C'
        },
        'Question 4' : {
            'Q' : 'When was FC bayern Munich established ?',
            'A' : '1891',
            'B' : '1900',
            'C' : '1904',
            'D' : '1895',
            'answer' : 'Option B'

        },
        'Question 5' : {
            'Q' : 'Who is the only foreign player to have been permanently appointed captain at Bayern ?',
            'A' : 'Bixente Lizarazu',
            'B' : 'Mark van Bommel',
            'C' : 'Niko Kovac',
            'D' : 'Robert Kovac',
            'answer' : 'option B'
        },
        'Question 6' : {
            'Q' : 'How many times did the club legend Oliver Kahn represent the German national team? ' ,
            'A' : '105',
            'B' : '86',
            'C' : '95',
            'D' : '65',
            'answer' : 'option B'
        },
        'Question 7' : {
            'Q' : 'Who is the youngest player to play for Die Bayern ?',
            'A' : 'Mehmet Scholl',
            'B' : 'Bastian Schweinsteiger',
            'C' : 'Pierre-Emile Højbjerg',
            'D' : 'David Alaba',
            'answer' : 'option C'
        },
        'Question 8' : {
            'Q' : 'Which of these Iranians never played for Bayern?',
            'A' : 'Ali Karimi',
            'B' : 'Mehdi Mahdavikia',
            'C' : 'Ali Daei',
            'D' : 'Vahid Hashemian',
            'answer' : ' Option B'
        },
        'Question 9' : {
            'Q' : 'Sepp Maier holds the record for the most consecutive games played in the Bundesliga, but how many games did he amass ?',
            'A' : '397',
            'B' : '402',
            'C' : '375',
            'D' : '422',
            'answer' : ' Option D'
        },
        'Question 10' : {
            'Q' : 'Bayern\'s record league victory was an emphatic 11 1 demolition in 1971. but who did they beat so convincingly ?',
            'A' : 'Borussia Dortmund',
            'B' : 'Arminia Bielefeld',
            'C' : 'MSV Duisburg',
            'D' : ' VFB Stuttgart',
            'answer' : ' Option A'
        },

    },

    'AC Milan' : {

        'Question 1' : {
            'Q' : 'When was AC Milan formed',
            'A' : '1900',
            'B' : '1889',
            'C' : '1899',
            'D' : '1901',
            'answer' : 'Opiton C'
       },
        'Question 2' : {
            'Q' : ' What is the nickname of AC Milan ? ',
            'A' : 'The Devil',
            'B' : 'The Red Devils',
            'C' : 'Elfs',
            'D' : 'The Sharks',
            'answer' : 'option A'

        },
        'Question 3' : {
            'Q' : 'What was the year of the first Scudetto for AC Milan ? ',
            'A' : '1899', 
            'B' : '1901',
            'C' : '1915',
            'D' : '1920',
            'answer' : 'option B'
        },
        'Question 4' : {
            'Q' : ' What is the name of the player who had the number 6 shirt and due to his devotement to the club, Milan have decided to retire that shirt forever ? ',
            'A' : 'Franco Baresi',
            'B' : 'Mauro Tosseti',
            'C' : 'Roberto Donadoni',
            'D' : 'Maldini',
            'answer' : 'Option A'

        },
        'Question 5' : {
            'Q' : 'AC Milan were the first Italian club to play in the European Cup. Who was their first opponent ?',
            'A' : 'Anderlecht',
            'B' : 'Real Madrid',
            'C' : 'Saarbrucken',
            'D' : 'Hibernian',
            'answer' : 'option C'
        },
        'Question 6' : {
            'Q' : 'Who is the oldest player to feature in an official match for AC Milan ?' ,
            'A' : 'Maldini',
            'B' : 'Gattuso',
            'C' : 'Sebastiano Rossi',
            'D' : 'Alessandro Costacurta',
            'answer' : 'option D'
        },
        'Question 7' : {
            'Q' : 'Which player holds the record for most goals scored in a single season for AC Milan ?',
            'A' : 'Andriy Shevchenko',
            'B' : 'Zlatan Ibrahimovic',
            'C' : 'Marco Van Basten',
            'D' : 'Gunnar Nordahl',
            'answer' : 'option D'
        },
        'Question 8' : {
            'Q' : 'Which AC Milan coach had the longest single spell in charge of the Rossoneri ?',
            'A' : 'Nereo Rocco',
            'B' : 'Fabio Capello',
            'C' : 'Carlo Ancelotti',
            'D' : 'Herbert Kilpin',
            'answer' : ' Option C'
        },
        'Question 9' : {
            'Q' : 'AC Milan became the first club to go an entire Serie A season without defeat en route to winning the 1991/92 Scudetto. Who was their coach?',
            'A' : 'Arrigo Sacchi',
            'B' : 'Nils Liedholm',
            'C' : 'Fabio Capello',
            'D' : 'Oscar Tabarez',
            'answer' : ' Option C'
        },
        'Question 10' : {
            'Q' : 'Who is the most expensive signing in AC Milan history ?',
            'A' : 'Alessandro Nesta',
            'B' : 'Filippo Inzaghi',
            'C' : 'Manuel Rui Costa',
            'D' : 'Ronaldinho',
            'answer' : ' Option C'
        },

    },

    'Inter Milan' : {

        'Question 1' : {
            'Q' : 'Internazionale\'s 17 Serie A titles puts them where on the list of Italy\'s most successful clubs ?',
            'A' : 'First',
            'B' : 'Second',
            'C' : 'Third',
            'D' : 'Fourth',
            'answer' : 'Opiton B'
       },
        'Question 2' : {
            'Q' : 'Inter are the only side never to have been relegated from Serie A. How many consecutive seasons have they spent in the top flight ?',
            'A' : '69',
            'B' : '78',
            'C' : '86',
            'D' : '92',
            'answer' : 'option B'

        },
        'Question 3' : {
            'Q' : 'With which team do Inter play the Derby d\'Italia ? ',
            'A' : 'Juventus', 
            'B' : 'Lazio',
            'C' : 'Napoli',
            'D' : 'AC Milan',
            'answer' : 'option A'
        },
        'Question 4' : {
            'Q' : ' Who did Inter beat in the 1964-65 European Cup final? ',
            'A' : 'Benfica',
            'B' : 'Real Madrid',
            'C' : 'Feyenoord',
            'D' : 'Barcelona',
            'answer' : 'Option A'

        },
        'Question 5' : {
            'Q' : 'Which of these players did not play for Inter, Juventus and Milan ?',
            'A' : ' Filippo Inzaghi',
            'B' : 'Giuseppe Meazza',
            'C' : 'Roberto Baggio',
            'D' : 'Christian Vieri',
            'answer' : 'option A'
        },
        'Question 6' : {
            'Q' : 'Giovanni Trapattoni won six titles as manager of Juventus. How many did he win with Inter ?' ,
            'A' : 'Zero',
            'B' : 'One',
            'C' : 'Two',
            'D' : 'Three',
            'answer' : 'option B'
        },
        'Question 7' : {
            'Q' : 'Prior to their victory in 2005-06, when was the last time Inter had won the league ?',
            'A' : '1971 72',
            'B' : '1982 83',
            'C' : '1988 89',
            'D' : '1990 01',
            'answer' : 'option C'
        },
        'Question 8' : {
            'Q' : 'AC Milan lengend Franco Baresi\'s brother played 559 times for Inter. What was his name ?',
            'A' : 'Roberto',
            'B' : 'Giacinto',
            'C' : 'Walter',
            'D' : 'Giuseppe',
            'answer' : ' Option D'
        },
        'Question 9' : {
            'Q' : 'In which year did Inter Milan sign Dennis Bergkamp from Ajax ?',
            'A' : '1993',
            'B' : '1995',
            'C' : '1994',
            'D' : '1996',
            'answer' : ' Option A'
        },
        'Question 10' : {
            'Q' : 'Which Inter Milan player was named MoM for the 2010 Champions League final?',
            'A' : 'Milito',
            'B' : 'Pandev',
            'C' : 'Eto\'o ' ,
            'D' : 'Sneijder',
            'answer' : ' Option A'
        },


    },

    'Borussia Dortmund' : {

        'Question 1' : {
            'Q' : 'In which stadium did Dortmund beat Juventus in the 1997 Champions League final ?',
            'A' : 'Olympiastadion',
            'B' : 'Sukru Saracoglu Stadium',
            'C' : 'Stadionul National',
            'D' : 'Stade Louis Two',
            'answer' : 'Opiton A'
       },
        'Question 2' : {
            'Q' : 'Which former Arsenal goalkeeper left the Gunners to join Dortmund ?',
            'A' : 'Rami Shaaban',
            'B' : 'Guillaume Warmuz',
            'C' : 'Alexander Manninger',
            'D' : 'Mart Poom',
            'answer' : 'option B'

        },
        'Question 3' : {
            'Q' : 'Which club did they sign Jakub Blaszczykowski from? ',
            'A' : 'Mainz 05', 
            'B' : 'Borussia Monchengladbach',
            'C' : 'Lech Poznan',
            'D' : 'Wisla Krakow',
            'answer' : 'option D'
        },
        'Question 4' : {
            'Q' : ' Who has made the most appearances for the club in the Bundesliga ? ',
            'A' : 'Lars Ricken',
            'B' : 'Christian Worns',
            'C' : 'Michael Zorc',
            'D' : 'Matthias Sammer',
            'answer' : 'Option C'

        },
        'Question 5' : {
            'Q' : 'Dortmund were managed by an Italian in the 1997-98 season. Who was it?',
            'A' : ' Arrigo Sacchi',
            'B' : 'Nevio Scala',
            'C' : 'Giovanni Trapattoni',
            'D' : 'Alberto Zaccheroni',
            'answer' : 'option B'
        },
        'Question 6' : {
            'Q' : 'Marco Reus and Kevin Großkreutz were both at Dortmund as scholars, but never turned professional there. Which club did the midfielders start their professional careers at ?' ,
            'A' : 'Vfl Osnabruck',
            'B' : 'Kickers Offenbach',
            'C' : 'Borussia Monchengladbach',
            'D' : 'Rot Weiss Ahlen',
            'answer' : 'option D'
        },
        'Question 7' : {
            'Q' : 'What is the Dortmund motto ?',
            'A' : 'More than a club',
            'B' : 'True love',
            'C' : 'unity and justice and freedom',
            'D' : 'wisdom from the flames',
            'answer' : 'option B'
        },
        'Question 8' : {
            'Q' : 'This not too tall Japanese offensive midfielder first wore Black & Yellow in 2010. Despite playing only two years for Borussia Dortmund (before moving on to Manchester United) his extraordinary skill has quickly won him the hearts of the fans. He played in the number 23 jersey. Who is he ? ',
            'A' : 'Shinji kagawa',
            'B' : 'Sakai',
            'C' : 'Okazaki',
            'D' : 'Miyagi',
            'answer' : ' Option A'
        },
        'Question 9' : {
            'Q' : 'This Turkish international defensive midfielder came to Borussia Dortmund in 2005 and left in 2011. After playing for Real Madrid and Liverpool FC he returned to the BVB in the winter of 2013, much to the delight of the Yellow Wall. He played in the number 18 jersey',
            'A' : 'Gundogan',
            'B' : 'Sahin',
            'C' : 'Bender',
            'D' : 'Frungs',
            'answer' : ' Option B'
        },
        'Question 10' : {
            'Q' : 'Which Dortmund player scored in the 2012-13 Champions League Final against Bayern Munich ?',
            'A' : 'Marco Reus',
            'B' : 'Ilkay Gundogan',
            'C' : 'Robert Lewandowski' ,
            'D' : 'Mats Hummels',
            'answer' : ' Option B'
        },

    },
}